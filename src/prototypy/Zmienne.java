package prototypy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class Zmienne {
	
	private class Wartosci {
		//wszystkie liczby u�yte w dzia�aniu
		private List<Double> zmienne;
		//wszystkie wyniki danych liczb z dzia�ania
		private Double wynik;
		//lista operacji u�ytych dla konkretnego dzia�ania
		private Arytmetyka dzialanie;
		
		//TODO
		//mo�na utworzy� konstruktory pozwalaj�ce na dodanie ju� gotowych warto�ci
		//oraz warto doda� konstruktor kopiuj�cy
		public Wartosci() {
			zmienne=new ArrayList<>();
			wynik=0d;
			dzialanie=Arytmetyka.Nieustalone;
		}
		
		public void setWynik(Double w) {
			this.wynik = w;
		}
		
		public Double getWynik() {
			return this.wynik;
		}
		
		public void setArytemtyka(Arytmetyka d) {
			this.dzialanie=d;
		}
		
		public Arytmetyka getArytmetyka() {
			return this.dzialanie;
		}
		
		public void setZmienna(double z) {
			if (zmienne==null) zmienne=new ArrayList<>();
			zmienne.add(z);
		}
		
		public void setZmienne(List<Double> zm) {
			this.zmienne=zm;
		}
		
		public List<Double> getZmienne() {
			return zmienne;
		}
		
		public double getZmienna(int i) {
			return (zmienne.size()>i) ? zmienne.get(i) : null;
		}
		
	}
	
	List<Wartosci> wartosci;
	
	//sumarycznie wszystkie liczby, wyniki oraz kody dzia�a� ��czy jedna zmienna-wska�nik -> indeks
	
	private int indeks;
	
	public Zmienne() {
		wartosci=new ArrayList<>();
		indeks = 0;
	}
	
	/*
	 * Listy dzia�aj� inaczej ni� tablice. Generalnie gromadz� dane lecz poprzez odpowiednie metody. W j�zyku Java
	 * nie ma jednoznacznej mo�liwo�ci wyci�gni�cia upatrzonego, konkretnego elementu z listy, a tym bardziej manipulowanie 
	 * nim (a przynajmniej niedowolone jest jawne manipulowanie).
	 * 
	 * W zwi�zku z powy�szym na pocz�tek musimy zbada� czy nasza g��wna lista (zmienne) w og�le ma odpowiedni� ilo��
	 * p�l w sobie. To mo�na sprawdzi� poprzez zmienne.get(index); je�eli get nie b�dzie mog�o zwr�ci� wskazanego indeksu
	 * oznacza� to b�dzie, �e ten po prostu nie istnieje. Dlatego te� zamiast PRZYPISYWA� warto�� b�dziemy j� DODAWA�
	 * poprzez metod� add(), tak jak to b�dzie mia�o miejsce w przyk�adzie na dole
	 */
	
	public void dodajDoTablicy(double wartosc) {		
		try {
			//sprawdzenie czy posiadamy wskazny indeks
			wartosci.get(indeks); 	
		}
		catch (Exception e) {
			//nie posiadamy czyli tworzymy nowa list� w ramach zmiennej przechowuj�cej wyniki w liscie
			wartosci.add(new Wartosci());
		}
		finally {
			//zniezale�nie od wykonania try...catch dodajemy now� warto�� do listy
			wartosci.get(indeks).setZmienna(wartosc); 
		}
	}
	
	public void dodajDoTablicy(String wartosc) {
		dodajDoTablicy(wartosc, false);
	}
	
//	public void dodajDoTablicy(Object wartosc) {
//		dodajDoTablicy(String.valueOf(wartosc), false);
//	}
	
	public void dodajDoTablicy(Object wartosc, boolean zmienna) {
		dodajDoTablicy( String.valueOf(wartosc), zmienna);
	}
	
	public void dodajDoTablicy(String wartosc, boolean zmienna) {
		try {			
			//je�eli mamy do czynienia ze zmienn� (zmienn� w sensie, i� u�ytkownik chce pobra� wynik operacji poprzedniego dzia�ania)
			//to pobieramy podci�g z warto�ci ALE z pomini�ciem pierwszego znaku (kt�ry b�dzie tutaj znakiem $). Numer po tym�e znaku
			//b�dzie wskazaywa� na numer dzia�ania, z kt�rego wynik chcemy u�y� w obecnym dzia�aniu
			//w pozosta�ych wypadkach wykona si� dodanie warto�ci przekazanej do funkcji (kod po dwukropku)
			dodajDoTablicy( (zmienna) ? wartosci.get(Integer.valueOf(wartosc.substring(1))).getWynik() : Double.valueOf(wartosc));
		}
		//je�eli try spowoduje b��d (bo np. nie b�dzie odpiwiedniego dzia�ania w li�cie) to po prostu nie dodamy warto�ci.
		catch(Exception ex) {
			return;
		}	
	}	
	
	public List<List<Double>> getZmienne() {
		List<List<Double>> zmienne = new ArrayList<>();
		for (Wartosci zm : wartosci) {
			zmienne.add(zm.getZmienne());
		}
		return zmienne;
	}
	
	/***
	 * Metoda ma za zadanie pobra� wszystkie liczby dzia�ania wskazanego przez parametr i
	 * @param i wprowadzone dzia�anie, kt�re elementy (liczby) chcemy wyci�gn��
	 * @return list� wszystkie element�w (liczby)
	 */
	public List<Double> getZmienne(int i) {
		return (indeks > i) ? wartosci.get(i).getZmienne() : null; 
	}
	
	/***
	 * Metoda pobiera ostatnio zapisany na li�cie ci�g liczb celem wykonania na nich dzia�a�
	 * @return zwraca tablic� wszystkich element�w (liczb), kt�re u�ytkownik wpisa� w konsoli
	 */
	public List<Double> getLastZmienne() {
		return wartosci.get(wartosci.size()-1).getZmienne();
	}
	
	public void ustawDzialanie(Arytmetyka a) {
		wartosci.get(indeks).setArytemtyka(a);
	}
	
//	public void dodajWynik(Object wynik) {
//		dodajWynik(Double.valueOf(String.valueOf(wynik)));
//	}
	
	public void dodajWynik(String wynik) {
	dodajWynik(Double.valueOf(wynik));
}
	
	public void dodajWynik(double wynik) {
		wartosci.get(indeks++).setWynik(wynik);
	}
	
	/***
	 * Metoda ma za zadanie pobra� WSZYSTKIE zmienne, wyniki oraz dzia�ania arytmetyczne i zwr�cienie ich w kolejnych
	 * listach (w ramach listy zbiorczej)
	 * @return
	 */
	public List<List<Double>> getWszystkieZmienne()	{
		List<List<Double>> zmienne = new ArrayList<>();
		for (int i  = 0; i < indeks; i++) {
			List<Double> tmp = new ArrayList<Double>(wartosci.get(i).getZmienne());
			//artymetyka ma charakterystyczn� warto�� - od (char)+ do (char)/; mo�na wi�c wnioskowa� kiedy ko�czymy wczytywa� warto�ci 
			//i kiedy powinien pojawi� si� wynik
			tmp.add((double)wartosci.get(i).getArytmetyka().getInt());
			tmp.add(wartosci.get(i).getWynik());			
			zmienne.add(tmp);
		}
		return zmienne;
	}
	
	public List<Map<String,Object>> getWszystkieZmienneMap() {
		List<Map<String,Object>> zmienne = new ArrayList<Map<String,Object>>();
		for (int i  = 0; i < indeks; i++) {
			Map<String,Object> ret = new HashMap<String,Object>();
			ret.put("wartosci", new ArrayList<Double>(wartosci.get(i).getZmienne()));
			ret.put("znak", wartosci.get(i).getArytmetyka().get(true));
			ret.put("wynik", wartosci.get(i).getWynik());
			zmienne.add(ret);
			//artymetyka ma charakterystyczn� warto�� - od (char)+ do (char)/; mo�na wi�c wnioskowa� kiedy ko�czymy wczytywa� warto�ci 
			//i kiedy powinien pojawi� si� wynik
			//tmp.add((double)wartosci.get(i).getArytmetyka().getInt());
			//tmp.add(wartosci.get(i).getWynik());			
			//zmienne.add(tmp);
		}
		return zmienne;
	}
}
