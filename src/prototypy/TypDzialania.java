package prototypy;

public enum TypDzialania {
	DodajWartosc(0),	//DODAJWARTOSC
	DodajWynik(1), 		//DODAJWYNIK
	DodajObwod(2),
	DodajPole(3),
	DodajDzialanie(4),
	Nieustalone(-1);
	
	private final int i;
	
	private TypDzialania(int i) { this.i=i; }
	
	public int get() { return i; }
	
	public static int get(TypDzialania typ) {
		return typ.get();
	}
	
	public static TypDzialania get(int i) {
		switch(i) {
			case 0: return TypDzialania.DodajWartosc;
			case 1: return TypDzialania.DodajWynik;
			case 2: return TypDzialania.DodajObwod;
			case 3: return TypDzialania.DodajPole;
			case 4: return TypDzialania.DodajDzialanie;
		}
		return TypDzialania.Nieustalone;
	}
}
