package prototypy;

public enum Arytmetyka {
		Dodawanie('+'),
		Odejmowanie('-'),
		Mnozenie('*'),
		Dzielenie('/'),
		Modulo('%'),
		Potega('^'),
		Silnia('!'),
		DodawanieInt(0),
		OdejmowanieInt(1),
		MnozenieInt(2),
		DzielenieInt(3),
		Nieustalone(' ');
		
		private final char z;
		
		private Arytmetyka(char z) {
			this.z=z;
		}
		
		private Arytmetyka(int i) {
			switch(i) {
				case 0: this.z='+'; break;
				case 1: this.z='-'; break;
				case 2: this.z='*'; break;
				case 3: this.z='/'; break;
				default: this.z=' ';
			}
		}
		
		public int getInt() {return (int)this.z; }
		
		public String get() { return " " + z + " "; }
		
		public char get(boolean ch) { return z; }
		
		public static Arytmetyka get(int i) {
			switch(i) {
				case 0: return Arytmetyka.Dodawanie;
				case 1: return Arytmetyka.Odejmowanie;
				case 2: return Arytmetyka.Mnozenie;
				case 3: return Arytmetyka.Dzielenie;
				case 4: return Arytmetyka.Modulo;
				case 5: return Arytmetyka.Potega;
				case 6: return Arytmetyka.Silnia;
			}
			return Arytmetyka.Nieustalone;
		}
		public static Arytmetyka get(char i) {
			switch(i) {
				case '+': return Arytmetyka.Dodawanie;
				case '-': return Arytmetyka.Odejmowanie;
				case '*': return Arytmetyka.Mnozenie;
				case '/': return Arytmetyka.Dzielenie;
				case '%': return Arytmetyka.Modulo;
				case '^': return Arytmetyka.Potega;
				case '!': return Arytmetyka.Silnia;
			}
			return Arytmetyka.Nieustalone;
		}
}
