package prototypy;

public interface PolaObwody {
	public void liczPole();
	public double getPole();
	public void liczObwod();
	public double getObwod();
}
