package prototypy;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public abstract class Dzialania {
	
//	protected double zmienne[][];
	
	Zmienne zmienne;
	
	//private String znaki[] = {" + ", " - ", " * "};
	
	protected Scanner skan;
	
	protected Dzialania() {
		this(new Scanner(System.in));
	}
	
	protected Dzialania(Scanner skan) {
		this.skan=skan;
		zmienne = new Zmienne();
	}
	//TODO 
	//Nie dzia�a znajdywanie pojedynczych cyfr; trzeba to naprawi�!
	protected void znajdzWartosci(String tekst) {
		if ((tekst.isEmpty() || tekst.isBlank()) && zmienne == null) {
			zmienne.dodajDoTablicy(0);
			return;
		}
		String tmp ="";
		tekst+=" ";
		boolean czyZmiennoprzecinkowa = false;
		boolean czyZmienna= false;
		for (char znak : tekst.toCharArray()) {

			//12.9
			if (znak >= '0' && znak <= '9')
				tmp+=znak;
			else if (czyZmiennoprzecinkowa) {
				if (!(tmp.charAt(tmp.length()-1) >= '0' && tmp.charAt(tmp.length()-1) <= '9')) 
					tmp=tmp.substring(0,tmp.length()-1);
				zmienne.dodajDoTablicy(tmp.replace(',', '.'));
				tmp="";
			}
			else if (czyZmienna) {
				
			}
			else if ((znak == '+' || znak=='-') && tmp.isEmpty()) 
				tmp+=znak;
			else if ((znak == '+' || znak=='-') && 
					(tmp.charAt(tmp.length()-1) == '+' || tmp.charAt(tmp.length()-1) == '-'))
				tmp=tmp.substring(0, tmp.length()-1)+znak;
			else if (znak == '$' && tmp.isEmpty()) {
				czyZmienna=true;
				tmp+=znak;
			}
			else if ((znak=='.' || znak==',') && !czyZmienna) {
				tmp+=znak;
				czyZmiennoprzecinkowa=true;
				//continue;
			}
			else {
				zmienne.dodajDoTablicy(tmp.replace(',', '.'));
				tmp="";
			}	
		}			 
	}
	
	//TODO
	//Poni�sza metoda powinna wy�wietla� nie tylko zmienne zawarte w obiekcie typu Zmienne, ale r�wnie� 
	//wyniki dzia�ania oraz typ dzia�ania (dodawania, odejmowanie, mno�enie... )
	protected void wypiszZmienne() {
		for(List<Double> zm : zmienne.getZmienne()) {
			System.out.println("Zapisane zmienne w kolejnym indeksie: ");
			for(double z : zm) {
				System.out.print(z + " ");
			}
			System.out.print('\n');
		}
	}
	
	protected void zerujZmienne() {
		zmienne=new Zmienne();		
	}
	
	protected String wypiszDzialanie(int opcja) {
		String wyjscie ="";
		for (double zm : zmienne.getLastZmienne()) {
			wyjscie+= zm + Arytmetyka.get(opcja).get();
		}
		return wyjscie.substring(0, wyjscie.length() - 3);
	}
	
	protected void wypiszTekst(String s, double wynik, int opcja) {
		System.out.println(s + " " + wypiszDzialanie(opcja) + " = " + wynik);
	}
	
	protected List<Double> getDzialanie() {
		return zmienne.getLastZmienne();
	}
	
	protected void dodajWartosc(double wartosc) {
		dodajWartosc(TypDzialania.DodajWartosc, wartosc);
	}
	
	protected void dodajWartosc(String wartosc) {
		dodajWartosc(TypDzialania.DodajWartosc, wartosc);
	}
	
	protected void dodajWartosc(int typ, Object wartosc) {
		dodajWartosc(TypDzialania.get(typ), wartosc);
	}
	
	protected void dodajWartosc(TypDzialania typ, Object wartosc) {
		if (typ == TypDzialania.DodajWartosc) {
			zmienne.dodajDoTablicy(String.valueOf(wartosc));
		}
		else if (typ == TypDzialania.DodajDzialanie) {
			try {
				zmienne.ustawDzialanie(Arytmetyka.get(Integer.valueOf(String.valueOf(wartosc))));
			}
			catch (Exception e) {
				zmienne.ustawDzialanie(Arytmetyka.get(String.valueOf(wartosc).charAt(0)));
			}
		}
		else if (typ == TypDzialania.DodajWynik) {
			zmienne.dodajWynik(String.valueOf(wartosc));
		}
	}
	
	protected boolean zapiszDoJSON() {
		return zapiszDoJSON("historia.json", 0);
	}
	
	
	protected boolean zapiszDoJSON(String plik) {
		return zapiszDoJSON(plik, 0);
	}
	
	protected boolean zapiszDoJSON(int tryb) {
		return zapiszDoJSON("historia.json", tryb);
	}
	
	
	protected boolean zapiszDoJSON(String plik, int tryb) {
		FileOutputStream fos=null;
		try {
			fos = new FileOutputStream(plik, tryb!=0);
			int indeks=0;
			fos.write("[{".getBytes());
			
			for(Map<String, Object> list : zmienne.getWszystkieZmienneMap()) {
				fos.write(("\"" + (indeks++) + "\": {").getBytes());
				@SuppressWarnings("unchecked")
				List<Double> zmienne = (List<Double>) list.get("wartosci");
				fos.write(("\"wartosci\": [").getBytes());
				String tmp="";
				for(double z : zmienne) {
					tmp+=String.valueOf(z) + ",";
				}
				fos.write(tmp.substring(0,tmp.length()-1).getBytes());
				fos.write("],".getBytes());
				fos.write(("\"znak\": \"" + list.get("znak")  +  "\",").getBytes() );
				fos.write(("\"wynik\": \"" + list.get("wynik")  +  "\"}").getBytes() );
			}
			fos.write("}]".getBytes());
			fos.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block			
			e.printStackTrace();
			return false;
		}	
		return true;
	}
	
	
	protected boolean odczytJSONPlik() {
		FileInputStream fis = null;
		String jsonIn = "";
		try {
			fis= new FileInputStream("historia.json");
			jsonIn = new String(fis.readAllBytes());
			fis.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(jsonIn);
		return true;
		//JSONObject obj = new JSONObject(jsonString);
	}
	
	
	protected boolean zapiszDoPliku() {
		return zapiszDoPliku("default.txt", 0);		
	}
	
	protected boolean zapiszDoPliku(String plik) {
		return zapiszDoPliku(plik, 0);
	}
	
	protected boolean zapiszDoPliku(int tryb) {
		return zapiszDoPliku("default.txt", tryb);
	}
	
	protected boolean zapiszDoPliku(String plik, int tryb) {
		FileWriter fw = null;
		try {
			fw = new FileWriter(plik, tryb!=0);
			for(List<Double> list : zmienne.getWszystkieZmienne()) {
				for (double wartosc : list) {
					fw.write(String.valueOf(wartosc) + " ");
				}
				fw.write('\n');
			}
			fw.close();			
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}		
		return true;
	}
	
	protected boolean odczytZPliku() {
		return odczytZPliku("default.txt", 0);
	}
	
	protected boolean odczytZPliku(String plik) {
		return odczytZPliku(plik, 0);
	}
	
	protected boolean odczytZPliku(int tryb) {
		return odczytZPliku("default.txt", tryb);
	}
	
	protected boolean odczytZPliku(String plik, int tryb) {
		FileReader fr = null;
		try {
			fr = new FileReader(plik);
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			
		}
		if (fr==null) return false;
		if (tryb==0) zerujZmienne();
		System.out.println("===ODCZYT Z PLIKU===");
		try {
			String tmp="";
			char znak=0;
			boolean wynik=false;
			int b=-1;
			while((b=fr.read())!=-1) {
				//wczytujemy znak; je�eli znak b�dzie inny ni� podane poni�ej
				if (b!=(int)' ' && b!=(int)';' && b!=(int)'\t' && b!=(int)'\r' && b!=(int)'\n') {
					//to dodajemy warto�� wczytan� (znak w postaci int) jako znak (char)
					//do zmiennej tmp (ci�g znakowy)
					tmp+=(char)b;
					//i kontynuujemy dodawanie (�eby reszta kodu si� nie wykona�a)
					continue;
				}
				//je�eli tutaj jeste�my to znaczy, �e kod if (ten powy�ej) si� nie wykona�; 
				//przypisujemy warto�c zapisan� w tmp do warto�ci znaku O ILE d�ugo�� naszego ci�gu wynosi 1
				//(czyli jeden znak) oraz warto�� zmiennej wynik jest false (odwr��enie warto�ci)
				if (tmp.length()==1 && !wynik) {
					//wrzucamy pierwszy (jedyny) znak z tmp do zmiennej znak
					znak=tmp.charAt(0);
					//jezeli jest on jednym ze znak�w arytmetycznych - wskazujemy, �e nast�pn� waerto�ci zapisan� w pliku powinien by�
					//wynik 
					if (Arytmetyka.Nieustalone != Arytmetyka.get(znak)) {
						wynik=true;
						tmp="";
						//aby reszta kodu metody si� nie wykona�a - u�ywamy continue
						continue;
					}
				}
				//je�eli jeste�my w tym miejscu i wynik ustawiony jest na warto�� true
				if (wynik) {
					//to sprawdzamy czy uda�o si� z pliku odczyta� jakikolwiek wynik (czy zosta� zapisany)
					//je�eli nie
					if (tmp.isEmpty() || tmp.charAt(0) == '\n' || tmp.charAt(0) == '\r') {
						//tutaj obliczmy dzialanie wedle podanego znaku
						//trzeba przerobi� dzia�ania w kalkulatorze tak by z tego miejsca metody artymetyczne by�y dost�pne
						tmp="0";
					}
					//poniewa� to koniec danego dzia�ania (niezale�nie czy liczyli�my wynik, czy nie)
					//dodajemy znak dzia�ania do naszej serii zmiennych
					dodajWartosc(TypDzialania.DodajDzialanie, znak);
					//dodajemy wynik do serii, tym samym ko�cz�c jedn� z serii (i przechodzimy ewentualnie dalej, do uzupe�niania kolejnych
					//serii).
					dodajWartosc(TypDzialania.DodajWynik, tmp);
					wynik=false;
				}
				else 
					//je�eli nie mamy do czynienia z wynikiem, nie mamy te� ustawionego znaku (zmienan wynik jest false)
					//to po prostu wrzucamy kolejne warto�ci do serii dzia�ania
					dodajWartosc(tmp);
				//czy�cimy zmienn� by m�c wstawi� kolejne warto�ci
				tmp="";
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			return false;
		}
		
		return true;
	}
}
