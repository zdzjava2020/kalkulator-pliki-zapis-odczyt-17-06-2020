package funkcjonalnosc;

import java.util.Scanner;

import prototypy.Dzialania;
import prototypy.Obliczenia;
import prototypy.TypDzialania;

public class Kalkulator extends Dzialania implements Obliczenia{

	public void menu() {
		int opcja=0;
		String opcje="\n\nWybierz jedn� z opcji:\n1.Dodawanie\n2.Odejmowanie\n3.Mno�enie\n"+
				"4.Dzielenie\n5.Reszta z dzielenia\n6.Pot�gowanie\n7.Pierwiastkowanie\n8.Wypisz zapisane wyniki"
				+ "\n9.Wyzeruj pami��\n10.Zapisz wyniki do pliku\n11.Konwertuj do pliku JSON\n"
				+ "12.Oczytaj z pliku\nDOWOLNA LICZBA - POWR�T DO PROGRAMU\n"+
							"Tw�j wyb�r: ";
		for(;;) {
			System.out.print(opcje);
			try {
				opcja=Integer.valueOf(skan.nextLine());				
			}
			catch (Exception ex) {
				System.err.println("Poda�e� z�� liczb�! Spr�buj jeszcze raz! (Wci�niej cokolwiek by kontynuowa�...)");
				skan.nextLine();
				continue;
			}
			
			switch(opcja) {
				case 1: 
				case 2:
				case 3: 
				case 4: 
				case 5:
				case 6: 
				case 7: wprowadzDzialanie(opcja-1);break;
				case 8: wypiszZmienne(); break;
				case 9: zerujZmienne(); break;
				case 10: zapiszDoPliku(); break;
				case 11: zapiszDoJSON(); break;
				case 12: 
					String str="";
					System.out.println("Podaj nazw� pliku lub pomi�, by wczyta� domy�lny plik");
					odczytZPliku(((str = skan.nextLine()).equals("\n")) ? str : "dzialania.txt");
					break;
				case 13: odczytJSONPlik() ; break;
				default: return;
			}
		}
	}

	private void wprowadzDzialanie(int opcja) {
		System.out.print("Wprowad� warto�ci, kt�re nast�pnie b�d� bra�y udzia� a liczeniu: ");
		znajdzWartosci(skan.nextLine()); 
		double wynik = 0;
		//ka�da z opcji musi by� -1 (np. dodawanie w menu to 1, st�d opcja == 0, 
		//odejmowanie to 2, st�d opcja == 1 itd.)
		switch (opcja) {
			case 0: wynik=dodawanie(getDzialanie().toArray()); break;
			case 1: wynik=odejmowanie(); break;
			case 2: wynik=mnozenie(getDzialanie().toArray()); break;
			case 3: wynik=dzielenie(); break;
		}
		wypiszTekst("Wynik dzia�ania:", wynik, opcja); 
		dodajWartosc(TypDzialania.DodajDzialanie, opcja);
		dodajWartosc(TypDzialania.DodajWynik, wynik);
	}

//	public double dodawanie() {
//		double wynik = 0;		
//		for(double zm : getDzialanie()) {				
//			wynik+=zm;
//		}
//		return wynik;
//	}
//	
	private double odejmowanie() {		
		double wynik = getDzialanie().get(0);
		for(int i=1;i<getDzialanie().size();i++) {				
			wynik-=getDzialanie().get(i);
		}
		return wynik;
		
	}
	
//	public double mnozenie() {		
//		double wynik = getDzialanie().get(0);
//		for(int i=1;i<getDzialanie().size();i++) {				
//			wynik*=getDzialanie().get(i);
//		}
//		return wynik;
//	}
	
//	private void modulo() {
//		
//	}
	
	public Kalkulator(Scanner skan) {
		super(skan);		
	}

	
	//teraz mog� doda� parametry przy wywo�aniu funkcji w taki spos�b:
	//dodawanie(12,15.6,-9,0,19)
	@Override
	public double dodawanie(Object... a) {
		double wynik = 0;		

		for(Object zm : a) {				

			wynik+= Double.valueOf(String.valueOf(zm));
		}
		return wynik;

	}
	//gdyby by�o 
	//public double dodawanie(Object[] a) {
	//trzeba by by�o dodawa� warto�ci tak: dodawanie({12,15.6,-9,0,19})

	@Override
	public double mnozenie(Object... a) {
		double wynik = Double.valueOf(String.valueOf(a[0]));
		for(int i=1;i<getDzialanie().size();i++) {				
			wynik*=getDzialanie().get(i);
		}
		return wynik;
	}
	
	public double dzielenie() {
		double wynik = getDzialanie().get(0);
		for(int i=1;i<getDzialanie().size();i++) {
			if (getDzialanie().get(i)!=0)
				wynik/=getDzialanie().get(i);
		}
		if (wynik == getDzialanie().get(0))
			wynik=Double.POSITIVE_INFINITY;
		return wynik;
	}
}
